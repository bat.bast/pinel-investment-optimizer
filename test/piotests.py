import logging

from selogerneuf.programparser import Program
from selogerneuf.searchparser import Search

url_program = 'https://www.selogerneuf.com/annonces/neuf/programme/creteil-94/119384467/'
insee_code = '940068'


def no_test_program_access():
    pg = Program(url_program)
    res = pg.get_real_estates()
    assert int(url_program.split('/')[-2]) == res[0]['programId']


def test_fetch_apptocken():
    sh = Search(insee_code)
    res = sh.get_programs()
    assert 0
