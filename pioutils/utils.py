import random, time, requests, logging, csv, random

constant_fake_ip = ".".join(map(str, (random.randint(0, 255) for _ in range(4))))

__request_session = None
__user_agents = []

# Source = https://raw.githubusercontent.com/yusuzech/top-50-user-agents/master/user_agent.csv
user_agent_file = 'User-Agent.csv'

def build_headers_selogerneuf():
    # Fake IP
    fake_ip = constant_fake_ip
    # Fake origin
    origin = 'https://www.selogerneuf.com'
    # Choose random UA
    global __user_agents
    if len(__user_agents)==0:
        with open(user_agent_file, 'r') as f:
            logging.info('Loading User-Agent list from %s...', user_agent_file)
            r = csv.DictReader(f, delimiter=',')
            for ua in r:
                __user_agents.append(ua['User agent'])

    ua = random.choice(__user_agents)
    # Browser parameters
    language = 'en-US,en;q=0.5'
    cache = 'no-cache'

    headers = {'User-Agent': ua, 'Referer': origin, 'Origin': origin, 'Via': fake_ip, 'X-Forwarded-For': fake_ip,
               'Accept-Language': language, 'Pragma': cache}
    return headers


def wait():
    # Wait a little before the next request
    time.sleep(random.randint(500, 2000) / 1000)


def request_session():
    global __request_session
    if __request_session is None:
        __request_session = requests.Session()
        headers = build_headers_selogerneuf()
        __request_session.get('https://www.selogerneuf.com', headers=headers)
    wait()
    return __request_session
