import logging, csv, configparser
from selogerneuf.searchparser import Search
from selogerneuf.programparser import Program
import pioutils.utils as piou

########## CONFIGURATION ##########
config_file = 'pio-config.ini'
###################################


__csv_file = 'pinel.csv'
# Source = https://www.data.gouv.fr/fr/datasets/liste-des-zones-ptz-et-pinel-1/
__ptz_file = 'Communes_PTZ.csv'


def main():
    # Read configuration
    logging.info("Reading configuration from %s...", config_file)
    config = configparser.ConfigParser()
    config.read(config_file)
    config_search = config['SEARCH']
    config_frais = config['FRAIS']
    config_gains = config['GAINS']
    config_pinel = config['PINEL']
    config_propgram = config['PROGRAM']

    # Load PTZ classification
    ptz = {}
    with open(__ptz_file, 'r') as f:
        logging.info('Loading PTZ classification from %s...', __ptz_file)
        r = csv.DictReader(f, delimiter=';')
        for city in r:
            ptz[city['Commune'].upper()] = city['PTZ'].upper()

    # First, fetch real properties for a region
    logging.info('Searching programs in INSEE Code %s...', config_search['CodeINSEE'])
    programs = Search(config_propgram['BasePATH'], config_search['CodeINSEE']).get_programs()
    nb_programs = len(programs)
    logging.info('Founded %s programs', str(nb_programs))

    # For each program, fetch real properties
    all_real_estates = []
    count = 0
    for pg in programs:
        pg['PTZ'] = ptz[pg['Ville'].upper()]
        count += 1
        logging.info('Analysing program "%s" (%s/%s)in %s (ID=%s ; PTZ=%s)...', pg['Nom'], str(count), str(nb_programs),
                     pg['Ville'], pg['IdProgramme'], pg['PTZ'])
        real_estates = Program(config_propgram['BasePATH'], pg['URL']).get_real_estates()
        if len(real_estates) == 0:
            logging.warning('\t- Founded %s real estates', str(len(real_estates)))
        else:
            logging.info('\t- Founded %s real estates', str(len(real_estates)))
        for real_e in real_estates:
            # Make Pinel calculus
            real_e['FraisDAchat'] = float(config_frais['FraisDAchat']) * float(real_e['Prix']) / 100
            real_e['PrixDeRevient'] = real_e['FraisDAchat'] + float(real_e['Prix'])
            real_e['CoutTotalDuCreditHorsAssurances'] = real_e['PrixDeRevient'] * float(
                config_frais['TauxDuCreditImmobilier']) / 100
            real_e['PrixDeRevientAvecCredit'] = real_e['PrixDeRevient'] + real_e['CoutTotalDuCreditHorsAssurances']
            real_e['SurfaceUtile'] = min(float(real_e['Surface']) + 8, float(real_e['Surface']) + (
                    float(real_e['SurfaceBalcon']) + float(real_e['SurfaceTerrasse'])) / 2)
            real_e['PrixDeRevienParM2Utile'] = real_e['PrixDeRevient'] / real_e['SurfaceUtile']
            real_e['PrixDeRevientPlafonneCalculParM2'] = min(
                float(config_pinel['PlafondPrixDeRevientM2Utile']) * real_e['SurfaceUtile'], real_e['PrixDeRevient'])
            real_e['PrixDeRevientPlafonneCalculTotal'] = min(float(config_pinel['PlafondPrixDeRevientTotal']),
                                                             real_e['PrixDeRevient'])
            real_e['EcartPlafondPinelCalculParM2'] = real_e['PrixDeRevient'] - real_e[
                'PrixDeRevientPlafonneCalculParM2']
            real_e['EcartPlafondPinelCalculTotal'] = real_e['PrixDeRevient'] - real_e[
                'PrixDeRevientPlafonneCalculTotal']

            duree_credit_immobilier = int(eval(config_frais['DureeCreditImmobilier']))
            duree_pinel = int(eval(config_pinel['DureePinel']))
            real_e['MensualiteCredit'] = (real_e['PrixDeRevient'] + real_e[
                'CoutTotalDuCreditHorsAssurances']) / duree_credit_immobilier

            real_e['AssuranceMensuelleCredit'] = float(config_frais['TauxAssuranceCredit']) * float(
                real_e['Prix']) / 100
            real_e['DefiscalisationMensuelle'] = float(config_pinel['TauxDefiscalisation']) / 100 * min(
                real_e['PrixDeRevientPlafonneCalculParM2'], real_e['PrixDeRevientPlafonneCalculTotal']) / 12
            real_e['CoefficientMultiplicateur'] = min(float(config_pinel['PlafondCoefficientMultiplicateur']),
                                                      0.7 + 19 / float(real_e['SurfaceUtile']))
            bareme = float(config_pinel['BaremeBrutPTZ_' + pg['PTZ']])
            real_e['RevenuLocatifMensuel'] = bareme * real_e['CoefficientMultiplicateur'] * float(
                real_e['SurfaceUtile'])
            real_e['ImpotMensuelSurRevenuLocatif'] = float(config_frais['TauxDeLaTrancheMarginaleImposition']) * real_e[
                'RevenuLocatifMensuel'] / 100
            real_e['ChargesProprietaireMensuelles'] = float(config_frais['TauxChargesProprietaire']) * real_e[
                'RevenuLocatifMensuel'] / 100
            real_e['AssuranceMensuelleImmobilierLocatif'] = float(config_frais['AssuranceImmobilierLocatif'])
            real_e['FraisDeGestionMensuel'] = float(config_frais['TauxFraisDeGestion']) * real_e[
                'RevenuLocatifMensuel'] / 100
            real_e['AssuranceMensuelleLoyesImpayes'] = float(config_frais['TauxAssuranceLoyesImpayes']) * real_e[
                'RevenuLocatifMensuel'] / 100
            real_e['TaxeFonciereMensuelle'] = float(config_frais['TaxeFonciere']) / 12
            real_e['FraisMensuelEntretienEtRefection'] = float(config_frais['FraisEntretienEtRefection']) / 12

            real_e['GainsMensuelsSiLoueTempsPLein'] = real_e['RevenuLocatifMensuel'] \
                                                      + real_e['DefiscalisationMensuelle']
            real_e['ChargesMensuellesSiLoueTempsPLein'] = real_e['MensualiteCredit'] \
                                                          + real_e['ImpotMensuelSurRevenuLocatif'] \
                                                          + real_e['AssuranceMensuelleCredit'] \
                                                          + real_e['ChargesProprietaireMensuelles'] \
                                                          + real_e['AssuranceMensuelleImmobilierLocatif'] \
                                                          + real_e['FraisDeGestionMensuel'] \
                                                          + real_e['TaxeFonciereMensuelle'] \
                                                          + real_e['AssuranceMensuelleLoyesImpayes'] \
                                                          + real_e['FraisMensuelEntretienEtRefection']
            real_e['SoldeMensuelSiLoueTempsPLein'] = real_e['GainsMensuelsSiLoueTempsPLein'] - real_e[
                'ChargesMensuellesSiLoueTempsPLein']

            real_e['GainsAnnuelsSiLoueTempsPLein'] = real_e['GainsMensuelsSiLoueTempsPLein'] * 12
            real_e['ChargesAnnuellesSiLoueTempsPLein'] = real_e['ChargesMensuellesSiLoueTempsPLein'] * 12
            real_e['SoldeAnnuelSiLoueTempsPLein'] = real_e['SoldeMensuelSiLoueTempsPLein'] * 12

            # On enlève le remboursement du crédit dans les charges (mais pas ses frais)
            real_e['TauxDeRentabilitéAnnuelSiLoueTempsPLein'] = (real_e['GainsAnnuelsSiLoueTempsPLein'] - real_e[
                'ChargesAnnuellesSiLoueTempsPLein'] + real_e['MensualiteCredit'] * 12) / real_e['PrixDeRevient'] * 100

            real_e['MontantRevente'] = (1 + float(config_gains['PlusValueRevente'])) * float(real_e['Prix'])
            reste_a_rembourser = (duree_credit_immobilier - duree_pinel) * real_e['MensualiteCredit']
            real_e['BilanSur' + str(int(duree_pinel / 12)) + 'AnsSiLoueTempsPlein'] = real_e[
                                                                                          'MontantRevente'] - reste_a_rembourser - \
                                                                                      real_e['FraisDAchat'] + real_e[
                                                                                          'SoldeMensuelSiLoueTempsPLein'] * duree_pinel
            real_e['TauxDeRentabilitéSur' + str(int(duree_pinel / 12)) + 'AnsSiLoueTempsPlein'] = real_e[
                                                                                                      'BilanSur' + str(
                                                                                                          int(
                                                                                                              duree_pinel / 12)) + 'AnsSiLoueTempsPlein'] / \
                                                                                                  real_e[
                                                                                                      'PrixDeRevient'] * 100
            all_real_estates.append(dict(pg, **real_e))

    # Save results
    if len(all_real_estates) > 0:
        csv_file = config_propgram['BasePATH'] + __csv_file
        with open(csv_file, 'w') as f:
            logging.info('Writing results into %s...', csv_file)
            w = csv.DictWriter(f, all_real_estates[0].keys(), delimiter=';')
            w.writeheader()
            for real_estate in all_real_estates:
                w.writerow(real_estate)


if __name__ == "__main__":
    logging.basicConfig(
        level=logging.INFO,
        format="[%(asctime)s - %(levelname)s] [%(name)s:%(lineno)s - %(funcName)20s()] %(message)s"
    )
    logging.info('===== START =====')
    main()
    logging.info('===== END =====   ')
