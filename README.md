# Pinel Investment Optimizer
## Recherche des biens immobiliers les plus rentables !

L'achat d'un bien immobilier via le dispositif Pinel permet _d'optimiser_ sa déclaration ficale.

La difficulté du choix a plusieurs origines :
- les simulateurs Pinel sont souvent faux et/ou demandent à laisser de trop nombreuses informations personnelles ;
- il existe des tableurs permettant ces calculs (souvent plus complets que les sites web) mais sans vision comparative des biens.

L'analyse se déroule en 3 étapes :
- Tout d'abord _scrapper_ un acteur bien connu du milieu (merci à eux pour le format de données particulièrement propre)
pour en retirer une liste de programmes immobiliers sur une zone précisée via les codes INSEE des communes.
- Ensuite, les calculs PINEL sont effectués (avec des erreurs ?) selon les paramétrages désirés (cf. pio-config.ini).
- Pour terminer, les résultats sont sauvegardés dans un fichier CSV, la dernière colonne indiquant le taux de rendement sur la durée du projet.
   - L'utiliser pour identifier le bien le plus rentable d'un point de vue calculatoire.

## Note
Le programme stocke les données récoltées dans un fichier :
- pour ne pas avoir à les récupérer systématiquement à chaque exécution ;
  - il suffit donc de supprimer l'un des fichiers pour que le script le récupère au prochain lancement ;
- une modification des paramètres et une relance du script permet un recalcul sans solliciter à nouveau le site web.

## Requirements

* python 3.7.x and some modules:
  * beautifulsoup4
  * requests

## Content
This repository includes the following files:

| File | Description |
|---|:---|
| pioutils | Configuration and generic tools |
| | |
| selogerneuf | Web scrapper for the web site www_dot_selogerneuf_dot_com ; OPS, please, don't read this code ;) |
| | |
| tests |  No test, it-s bad |
| | |
| analyse-real-properties.py | Launcher where you can adjust Log level and INSEE Code for the search |
| | |
| Communes_PTZ.csv | https://www.data.gouv.fr/fr/datasets/liste-des-zones-ptz-et-pinel-1/ |
| | |
| User-Agent.csv | https://raw.githubusercontent.com/yusuzech/top-50-user-agents/master/user_agent.csv |
| | |
| pio-config.ini | Configuration file, adjust financial parameters |

## Issues
Please report any bugs or requests that you have.

This is a Free project used to practice Python, there is no support ;)

## Licence
GNU AFFERO GENERAL PUBLIC LICENSE Version 3

## Contributing
bat.bast@boiteataquets.org