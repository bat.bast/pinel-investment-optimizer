import logging, re, json, os
from bs4 import BeautifulSoup
import pioutils.utils as piou

string_find_program_details_json = re.compile('programDetail')
re_program_details_json_details_json = re.compile(r'.+programDetail = (\{.+logements : (\[.+\]))')
string_find_program_nb_real_estates = re.compile('Programme de')
re_program_program_nb_real_estates = re.compile(r'Programme de (\d+) logements')


class Program:
    def __init__(self, base_path: '/tmp/', url: None):
        self.url = url
        self.__nb_total_real_estates = 0
        self.__base_path = base_path
        self.__real_estates = self.__build_program_details()

    def __fetch_program_page(self):
        html = None
        filename = self.__base_path + 'PIO_Program_' + self.url.split('/')[-2] + '.html'
        if os.path.isfile(filename):
            with open(filename, 'r') as content_file:
                content = str.encode(content_file.read())
                html = BeautifulSoup(content, "html.parser")
        else:
            headers = piou.build_headers_selogerneuf()
            logging.debug('Request: %s\nHeaders = %s', self.url,
                          '\n'.join('{}: {}'.format(k, headers[k]) for k in headers))
            req = piou.request_session().get(self.url, headers=headers)
            if req is not None and req.status_code == 200:
                page = req.content
                html = BeautifulSoup(page, "html.parser")
                with open(filename, 'w') as content_file:
                    content_file.write(page.decode())
                logging.debug('Response:\n%s', html.prettify())

        return html

    def __build_program_details(self):
        page = self.__fetch_program_page()
        details = []
        if page is not None:
            # Fetch JSON inside Javascript
            data = page.find('script', type="text/javascript", string=string_find_program_details_json)
            if data is not None:
                tag = ' '.join(data.text.replace('\r\n', ' ').replace('\n', ' ').replace('\t', '').split())
                res = re_program_details_json_details_json.match(tag)
                if res is not None:
                    details = json.loads(str(res.groups()[1]))
                    logging.debug(json.dumps(details, indent=4, sort_keys=True))
            # Fetch nb real estates from HTML tag
            data = page.find('p', class_="summarySmall", string=string_find_program_nb_real_estates)
            if data is not None:
                tag = data.text
                res = re_program_program_nb_real_estates.match(tag)
                if res is not None:
                    self.__nb_total_real_estates = int(res.groups()[0])
                    logging.debug(res)

        return details

    def get_real_estates(self):
        real_estates = []
        for rp in self.__real_estates:
            orientation_text = 'N' if rp['SiNord'] else ''
            if rp['SiSud']:
                orientation_text += 'S'
            if rp['SiEst']:
                orientation_text += 'E'
            if rp['SiOuest']:
                orientation_text += 'O'
            commentaire = ''
            if rp['NbPieces'] is not None:
                nbpieces = rp['NbPieces']
            else:
                nbpieces = 1
                commentaire = "Nombre de pièces absent "
            if rp['Surface'] is not None:
                surface = rp['Surface']
            else:
                surface = int(nbpieces) * 18
                commentaire += "Surface absente "
            if rp['Prix']['Prix'] is not None and float(rp['Prix']['Prix']) > 0:
                prix = rp['Prix']['Prix']
                real_estate = {'Id': rp['Id'],
                               'NombreTotalDeLogements': self.__nb_total_real_estates,
                               'Prix': prix,
                               'NbPieces': nbpieces,
                               'Surface': surface,
                               'Etage': rp['Etage'],
                               'SiJardin': rp['SiJardin'],
                               'Orientation': orientation_text,
                               'NbBalcons': rp['NbBalcons'],
                               'NbTerrasses': rp['NbTerrasses'],
                               'SurfaceBalcon': rp['SurfaceBalcon'] if rp['SurfaceBalcon'] is not None else 0,
                               'SurfaceTerrasse': rp['SurfaceTerrasse'] if rp['SurfaceTerrasse'] is not None else 0,
                               'SurfaceJardin': rp['SurfaceJardin'] if rp['SurfaceJardin'] is not None else 0,
                               'PrixM2': rp['PrixM2'].replace(' ', '').replace('€', ''),
                               'TypeDeBien': rp['TypeDeBien'],
                               'Commentaire': commentaire}
                real_estates.append(real_estate)
        return real_estates
