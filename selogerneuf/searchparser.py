import logging, json, os
import pioutils.utils as piou

json_api_url = 'https://api-sln.svc.groupe-seloger.com/v2.0/programs/search/'
search_url = 'https://www.selogerneuf.com/recherche'


class Search:
    def __init__(self, base_path: '/tmp/', insee_code: '940068'):
        self.insee_code = insee_code
        self.__base_path = base_path
        self.__programs = self.__fetch_programs()

    def __fetch_apptoken(self):
        headers = piou.build_headers_selogerneuf()
        payload = {'idtypebien': '1,2,9,4', 'idtt': '9', 'ci': '[' + self.insee_code + ']'}
        logging.debug('Request: %s\nPayload = %s\nHeaders = %s', search_url,
                      '\n'.join('{}: {}'.format(k, payload[k]) for k in payload),
                      '\n'.join('{}: {}'.format(k, headers[k]) for k in headers))
        req = piou.request_session().get(search_url, headers=headers)

        logging.debug('Response:\n%s', req.content)
        if req is not None and req.status_code == 200:
            apptoken = req.cookies['leuKi']
            logging.debug('AppTocken: %s', apptoken)
        else:
            apptoken = None

        return apptoken

    def __fetch_programs(self):
        details = None
        filename = self.__base_path + 'PIO_Search_' + self.insee_code.replace(',', '_') + '.json'
        if os.path.isfile(filename):
            with open(filename, 'r') as content_file:
                content = content_file.read()
                details = json.loads(content)
        else:
            headers = piou.build_headers_selogerneuf()
            headers['AppToken'] = self.__fetch_apptoken()

            if headers['AppToken'] is not None:
                headers['Content-Type'] = 'application/json'
                payload = '{"query":{"propertyTypeId":[1,2,9],"transactionTypeId":9,"inseeCode":[' + self.insee_code + ']},"allowEnlargeSearch":true,"orderBy":0,"page":1,"items":100}'
                logging.debug('Request: %s\nPayload = %s\nHeaders = %s', json_api_url, payload,
                              '\n'.join('{}: {}'.format(k, headers[k]) for k in headers))

                req = piou.request_session().post(json_api_url, data=payload, headers=headers)

                if req is not None and req.status_code == 200:
                    details = json.loads(req.content)['programs']
                    with open(filename, 'w') as content_file:
                        content_file.write(json.dumps(details))
                    logging.debug(json.dumps(details, indent=4, sort_keys=True))
        return details

    def get_programs(self):
        programs = []
        if self.__programs is not None:
            for pg in self.__programs:
                if pg['coordinates'] is not None:
                    latitude = pg['coordinates']['lat']
                    longitude = pg['coordinates']['lon']
                else:
                    latitude = 0
                    longitude = 0

                program = {'Nom': pg['name'],
                           'IdProgramme': pg['programId'],
                           'Adresse': pg['address'],
                           'Ville': pg['city'],
                           'NiveauGBB': pg['levelGBB'],
                           'Latitude': latitude,
                           'Longitude': longitude,
                           'DateCreation': pg['creationDate'],
                           'URL': pg['programUrl'],
                           'CodePostal': pg['zipCode']}
                programs.append(program)
        return programs
